using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptLifecycleStudy : MonoBehaviour
{
    void Awake()
    {
         Debug.Log("Awake() has been called.");
         }

 // Use this for initialization
 void Start()
    {
         Debug.Log("Start() has been called.");
         }

 // Update is called once per frame
 void Update()
    {
        
 }

 void OnDisable()
    {
         Debug.Log("OnDisable() has been called.");
         }

 void OnDestroy()
 {
 Debug.Log("OnDestroy() has been called.");
 }

 void OnApplicationPause()
 {
     Debug.Log("OnApplicationPause() has been called.");
     }

 void OnApplicationQuit()
 {
     Debug.Log("OnApplicationQuit() has been called.");
    }
}